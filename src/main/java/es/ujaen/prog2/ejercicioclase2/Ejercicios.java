/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.ejercicioclase2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author UJA
 */
public class Ejercicios {

    public static void ejercicio1() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Introduca los céntimos a devolver: ");

        int cents = sc.nextInt();
        int monedas[] = {50, 20, 10, 5, 2, 1};

        for (int i = 0; i < monedas.length; i++) {
            if (cents >= monedas[i]) {
                int aux = cents / monedas[i];
                System.out.println(aux + " monedas de " + monedas[i] + " céntimos.");
                cents = cents % monedas[i];
            }

        }

    }

    public static void ejercicio2() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Introduca el ancho: ");
        int ancho = sc.nextInt();
        System.out.println("Introduca el alto: ");
        int alto = sc.nextInt();

        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                if (i == 0 || j == 0 || i == (alto - 1) || j == (ancho - 1)) {
                    System.out.print("+");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println("");
        }

    }

    public static void ejercicio3() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Introduca la palabra: ");
        String palabra = sc.nextLine();

        System.out.println("Introduca el carácter: ");
        char letra = sc.nextLine().charAt(0);

        ArrayList<Integer> posiciones = new ArrayList<>();

        for (int i = 0; i < palabra.length(); i++) {
            if (palabra.charAt(i) == letra) {
                posiciones.add(i);
            }
        }
        
        if(posiciones.size() == 0){
            System.out.println("Ningún acierto. Pierdes un punto");
        }else{
            System.out.print(posiciones.size() + " acierto/s en las posicion/es ");
            for (Integer posicion : posiciones) {
                System.out.print(posicion + " ");
            }
        }
        System.out.println("");
    }

}
